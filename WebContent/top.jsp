<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   		<link href="./css/style.css" rel="stylesheet" type="text/css">
        <title>簡易Twitter</title>
        <script src="./js/jquery-3.6.0.min.js"></script>
	    <script src="./js/jquery-3.6.0.js"></script>
	    <script src="./js/main.js"></script>
    </head>
    <body>
        <div class="main-contents">
            <div class="header">
            	<c:if test="${ empty loginUser }">
		       		<a href="login">ログイン</a>
		        	<a href="signup">登録する</a>
		    	</c:if>
		    	<c:if test="${ not empty loginUser }">
			        <a href="./">ホーム</a>
			        <a href="setting">設定</a>
			        <a href="logout">ログアウト</a>
		    	</c:if>
            </div>

            <form action="./" method="get">
            	日付：
            	<input type="date" name="startDate" value="<c:out value="${ startDate }" />">
            	～
            	<input type="date" name="endDate" value="<c:out value="${ endDate }" />">
            	<input type="submit" value="絞込">
            </form>

            <c:if test="${ not empty loginUser }">
            	<div class="profile">
	            	<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
	            	<div class="account">@<c:out value="${loginUser.account}" /></div>
            		<div class="description"><c:out value="${loginUser.description}" /></div>
            	</div>
            </c:if>

			<c:if test="${ not empty errorMessages }">
			    <div class="errorMessages">
			        <ul>
			            <c:forEach items="${errorMessages}" var="errorMessage">
			                <li><c:out value="${errorMessage}" />
			            </c:forEach>
			        </ul>
			    </div>
			    <c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="form-area">
			    <c:if test="${ isShowMessageForm }">
			        <form action="message" method="post">
			            いま、どうしてる？<br />
			            <textarea name="text" id="message-area" cols="100" rows="5" class="tweet-box"></textarea>
			            <br />
			            <p>文字数： <span id="countUp">0</span></p>
			            <input type="submit" value="つぶやく">（140文字まで）
			        </form>
			    </c:if>
			</div>

			<div class="messages">
			    <c:forEach items="${messages}" var="message">
			        <div class="message">
			            <div class="account-name">
							<a href="./?user_id=<c:out value="${message.userId}"/>">
			                	<span class="account"><c:out value="${message.account}" /></span>
							</a>
			                <span class="name"><c:out value="${message.name}" /></span>
			            </div>
			            <div class="text"><pre><c:out value="${message.text}" /></pre></div>
			            <div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

			            <c:if test="${ loginUser.id == message.userId }">
			            	<div style="display:inline-flex">
								<form action="edit" method="get" style="margin-right: 10px;">
									<input name="message_id" value="${message.id}" id="id" type="hidden"/>
									<input type="submit" value="編集">
								</form>
								<form action="deleteMessage" method="post" style="margin-left: 10px;">
									<input name="message_id" value="${message.id}" id="id" type="hidden"/>
									<input type="submit" value="削除" id="delete-btn">
								</form>
							</div>
						</c:if>

  						<c:if test="${ not empty loginUser }">
							<form action="comment" method="post">
								返信<br />
				            	<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
				            	<input name="user_id" value="${loginUser.id}" id="id" type="hidden"/>
				            	<input name="message_id" value="${message.id}" id="id" type="hidden"/>
				            	<br />
				            	<input type="submit" value="返信">
							</form>
						</c:if>


						<c:forEach items="${comments}" var="comment">
							 <c:if test="${ message.id == comment.messageId }">
							 	<div class="account-name">
					                <span class="account"><c:out value="${comment.account}" /></span>
					                <span class="name"><c:out value="${comment.name}" /></span>
					            </div>
					            <div class="text" style=font-size:5px;><pre><c:out value="${comment.text}" /></pre></div>
					            <div class="date" style=font-size:3px;><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
							</c:if>
						</c:forEach>
			        </div>
			    </c:forEach>
			</div>

			<div id="top-btn"><a href="#">TOP</a></div>

            <div class="copyright"> Copyright(c)YourName</div>
        </div>
    </body>
</html>