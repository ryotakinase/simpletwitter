//つぶやき削除時のアラート
$(function(){
	$('#delete-btn').on('click', function(){
		alert('本当に削除してもよろしいですか？');
	});
});


//Topへスクロールするボタン
$(function(){
	$('#top-btn').hide();
	$(window).scroll(function(){
		if($(this).scrollTop() > 100){
			$('#top-btn').fadeIn();
		}else{
			$('#top-btn').fadeOut();
		}
	})
	$('#top-btn').on('click', function(){
		$('body, html').animate({ scrollTop: 0},
			500);
		return false;
	});
});


//つぶやき文字カウンター
$(function () {
	$('#message-area').keyup(function(){
		var counter = $(this).val().length;
		$('#countUp').text(counter);

		if(counter == 0){
			$('#countUp').text('0');
		}
		if(counter > 140){
			$('#countUp').css('color','red');
			$('#countUp').css('background-color','orange');
		} else if(counter == 140){
			$('#countUp').css('color','black');
		} else {
			$('#countUp').css('color','blue');
			$('#countUp').css('background-color','skyblue');
		}
	});
});